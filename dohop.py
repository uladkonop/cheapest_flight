from urllib.parse import urljoin
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import requests


class Dohop():

    BASE_URL = 'https://partners-api.dohop.com/api/v3/ticketing/dohop-connect/DE/EUR/'

    def __init__(self, params, logger):
        self.airport_from = params['depature-from']
        self.airport_to = params['arriving-to']
        self.departure_date = params['depature-date']

        if params.get('return-date'):
            self.return_date = params['return-date']

        self.logger = logger

    def build_data(self):
        json_data = self._requests_get()

        return self._parse_data(json_data)

    def _parse_data(self, json_data):
        data = []
        itineraries = json_data['itineraries']

        for i in itineraries:
            prepared_item = {}
            prepared_item['price'] = i['fare-combinations'][0]['fare-including-fee']['amount']
            prepared_item['flights-details'] = []

            for flight_out in i['flights-out']:
                flight_out = flight_out[0]
                flight_details = {}

                flight_details['flight-number'] = flight_out[2]
                flight_details['depature-date-time'] = flight_out[3]
                flight_details['arrival-date-time'] = flight_out[4]
                prepared_item['flights-details'].append(flight_details)

            data.append(prepared_item)

        return data

    def _requests_get(self):
        session = requests.Session()
        retries = Retry(total=5, backoff_factor=0.1, status_forcelist=[500, 503])
        session.mount('https://', HTTPAdapter(max_retries=retries))

        url = urljoin(self.BASE_URL, self._create_url_params())

        response = session.get(
            url, params={
                'ticketing-partner': '36fd0d405f4541b7be72d117b574a70f'})
		
        log_params = {
            "response_headers": response.headers,
            "response_body": response.content,
            "url": url,
            "request_headers": session.headers
        }

        self.logger.log(log_params)

        response.raise_for_status()

        return response.json()

    def _create_url_params(self):
    	if hasattr(self, 'return_date'):
    		return f'{self.airport_from}/{self.airport_to}/{self.departure_date}/{self.return_date}'
    	else:
            # breakpoint()
            return f'{self.airport_from}/{self.airport_to}/{self.departure_date}'