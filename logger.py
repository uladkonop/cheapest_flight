class Logger():
    def __init__(self, filename):
        self.filename = filename

    def log(self, params):
        log_record = self._format_log_record(params)
        
        with open(self.filename, "a+") as write_file:
            write_file.write(log_record)
    
    def _format_log_record(self, params):
        response_str = f'response: headers: {params["response_headers"]} body: {params["response_body"]}'
        request_str = f'request: url: {params["url"]} headers: {params["request_headers"]}'

        return request_str + '\n' +response_str