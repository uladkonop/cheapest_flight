from dohop import Dohop
from kiwi import Kiwi

from param_parser import ParamParser

from logger import Logger

from datetime import datetime, date, time

import json

from currency_converter import Converter


class Runner():

    LOG_FILE_NAME = 'log.log'
    OUTER_JSON_FILE = 'data_file.json'

    def __init__(self):
        self.logger = Logger(self.LOG_FILE_NAME)
        self.converter = Converter()

        search_params = ParamParser().parse()

        self.dohop = Dohop(search_params, self.logger)
        self.kiwi = Kiwi(search_params, self.logger)

    def run(self):
        dahop_flights = self.dohop.build_data()
        kiwi_flights = self.kiwi.build_data()

        all_flights = dahop_flights + kiwi_flights

        cheapest_flight = min(all_flights, key=lambda x: x['price'])

        def to_date(x): return datetime.strptime(
            x['flights-details'][0]['depature-date-time'], '%Y-%m-%d %H:%M')
        sorted_by_date_flights = sorted(all_flights, key=to_date)

        for flight in all_flights:
            flight['price'] = self.converter.convert(flight['price'])

        outer_json = {}
        outer_json['cheapest_flight'] = cheapest_flight
        outer_json['other_flights'] = sorted_by_date_flights[0:9]

        print(outer_json)

        self._dumb_json(outer_json)

    def _dumb_json(self, outer_json):
        with open("data_file.json", "w") as write_file:
            json.dump(outer_json, write_file)
