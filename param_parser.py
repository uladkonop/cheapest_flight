from sys import argv

class ParamParser():
    
    def parse(self):
        params = {}

        params['depature-from'] = argv[1]
        params['arriving-to'] = argv[2]
        params['depature-date'] = argv[3]

        if len(argv) == 5 : params['return-date'] = argv[4]

        return params

