import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
from urllib.parse import urljoin
from datetime import datetime, date, time

import xml.etree.ElementTree as ET

class Converter():
    
    URL = "http://www.nbrb.by/Services/XmlExRates.aspx?today"
    CURRENCY = "EUR"
    
    def convert(self, amount):
        element_tree = self._parse_xml()
        rate = self._find_rate(element_tree, self.CURRENCY)

        return amount * float(rate)


    def _parse_xml(self):
        xml_str = requests.get("http://www.nbrb.by/Services/XmlExRates.aspx?today").text
        return ET.ElementTree(ET.fromstring(xml_str))

    def _find_rate(self, element_tree, currency):
        for element in element_tree.findall(".//Currency"): 
            if element.find('./CharCode').text == currency:
                return element.find('./Rate').text
