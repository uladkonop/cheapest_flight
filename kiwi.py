import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
from urllib.parse import urljoin
from datetime import datetime, date, time

class Kiwi():

    BASE_URL = 'https://api.skypicker.com/'

    def __init__(self, params, logger):
        self.airport_from = params['depature-from']
        self.airport_to = params['arriving-to']
        self.departure_date = self._convert_date(params['depature-date'])
        if params.get('return-date') : self.return_date = self._convert_date(params['return-date'])

        self.logger = logger

    def build_data(self):
        json_data = self._requests_get()

        return self._parse_data(json_data)

    def _parse_data(self, json_data):
        data = []

        itineraries = json_data['data']

        for i in itineraries:
            prepared_item = {}
            prepared_item['price'] = i['price']
            prepared_item['flights-details'] = []

            for route in i['route']:
                flight_details = {}
                flight_details['flight-number'] = route['airline'] + \
                    str(route['flight_no'])
                flight_details['depature-date-time'] = str(
                    datetime.fromtimestamp(route['dTime']))[0:16]
                flight_details['arrival-date-time'] = str(
                    datetime.fromtimestamp(route['aTime']))[0:16]
                prepared_item['flights-details'].append(flight_details)

            data.append(prepared_item)

        return data

    def _requests_get(self):
        session = requests.Session()
        retries = Retry(total=5, backoff_factor=0.1, status_forcelist=[500, 503])
        session.mount('https://', HTTPAdapter(max_retries=retries))
        
        url = urljoin(self.BASE_URL, self._create_url_params())
        
        response = session.get(url)
        response.raise_for_status()

        log_params = {
            "response_headers": response.headers,
            "response_body": response.content,
            "url": url,
            "request_headers": session.headers
        }

        self.logger.log(log_params)

        return response.json()

    def _create_url_params(self):
    	if hasattr(self, 'return_date'):
    		return f'flights?flyFrom={self.airport_from}&to={self.airport_to}&dateFrom={self.departure_date}&dateTo={self.return_date}&partner=picky'
    	else:
            return f'flights?flyFrom={self.airport_from}&to={self.airport_to}&dateFrom={self.departure_date}&partner=picky'

    def _convert_date(self, rawdate):
        date = datetime.strptime(rawdate, '%Y-%m-%d')

        return date.strftime('%d/%m/%Y')