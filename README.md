**the task is:**

**1.  to find the cheapest flight between city pairs using two different flight search APIs.**

  Required output is
   *  recordings of raw requests and responses from each API
   *  json formatted data containing the cheapest itinerary between cities.

  Itinerary representation human-readable formatted should include:
   *  price
   *  flights details:
         flight numbers, dates and time of each flight.

**2. to sort all itineraries received from both APIs by departure time.**

   Required output is first 10 itineraries.
        
**3. to convert price in BYN. Exchange rate should be fetched from (please, use XML) nbrb.by** 

   http://www.nbrb.by/statistics/Rates/XML 

   Example: http://www.nbrb.by/Services/XmlExRates.aspx?ondate=05/02/2019 

**4. to prepare resulting script for running with arguments:**

   *  airport from
   *  airport to
   *  departure date
   *  return date (optional argument)
   
======

*So for example script running could look like:*
    `python your_script.py KEF NCE 2019-08-13`

Note all test cases are for single adult only, currency = EUR,
market/residency = DE and language = en

The APIs are:

   *  Dohop ticketing API - api.dohop.com 

   *  Kiwi API - see docs.kiwi.com

Any Python modules can be used to solve this task.